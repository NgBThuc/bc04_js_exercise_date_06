// Tìm số nguyên dương nhỏ nhất
// Sao cho: 1 + 2 + ... + n > 10000

function findSmallestInteger() {
  let sum = 0;
  let integer = 0;
  while (sum <= 10000) {
    integer++;
    sum += integer;
  }
  document.querySelector(
    "#js_ex1__result"
  ).textContent = `Số nguyên dương nhỏ nhất thỏa điều kiện: ${integer}`;
}
