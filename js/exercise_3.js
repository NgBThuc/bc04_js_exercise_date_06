/* 
Nhập vào N. Tính giai thừa 1 * 2 * ... * n
*/

function calcFactorial() {
  let n = document.querySelector("#js_ex3__num-n").value * 1;
  let factorialResult = 1;

  for (let i = 1; i <= n; i++) {
    factorialResult *= i;
  }

  document.querySelector(
    "#js_ex3__result"
  ).textContent = `Kết quả: ${new Intl.NumberFormat("VN-vn").format(
    factorialResult
  )}`;
}
