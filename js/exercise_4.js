/* 
Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div. Nếu div nào ở vị trí chẵn thì background màu đỏ và lẻ thì background màu xanh
*/

function printDiv() {
  for (let i = 1; i <= 10; i++) {
    if (i % 2 === 0) {
      document.querySelector(
        "#js_ex4__result"
      ).innerHTML += `<div class="bg-danger text-white p-2">Số chẵn</div>`;
    } else {
      document.querySelector(
        "#js_ex4__result"
      ).innerHTML += `<div class="bg-primary text-white p-2">Số lẻ</div>`;
    }
  }
}
