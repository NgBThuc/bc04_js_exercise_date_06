// Viết chương trình nhập vào 2 số x, n tính tổng:
// S(n) = x + x^2 + x^3 + ... + x^n (Sử dụng vòng lặp và hàm)

function calcSum() {
  let x = document.querySelector("#js_ex2__num-x").value * 1;
  let n = document.querySelector("#js_ex2__num-n").value * 1;
  let sum = 0;

  for (let i = 1; i <= n; i++) {
    sum += x ** i;
  }

  document.querySelector(
    "#js_ex2__result"
  ).textContent = `Tổng: ${new Intl.NumberFormat("VN-vn").format(sum)}`;
}
